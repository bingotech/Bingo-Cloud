# Bingo
中文冰果，一种传说中生长在极寒之地的智慧果实，融天地之精华，得到它可以成功渡过三五大劫，成就无上大神！

# 使命
让数字成为最有价值的资本！

# 愿景
打造科技人员穿越职业成长周期的圣地！

# 主题
金融科技、数字智能、职业成长、人生目标

# 文档
1. Spring Security认证基本原理 https://www.jianshu.com/p/f3a0fb302ef1
2. Spring Security OAuth2.0自定义登录页面 + JWT Token配置 https://www.jianshu.com/p/122dace5d570
3. Spring Secutity 添加过滤器实现自定义登录认证 https://www.jianshu.com/p/68885d0e1cd9
4. Spring Security 自定义生成JWT Token API https://www.jianshu.com/p/afed86fb90bb